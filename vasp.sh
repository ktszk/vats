#!/bin/env qsub
#$ -S /bin/bash
#$ -cwd
#$ -V
#$ -N La+2000
#$ -j Y
#$ -e out.o
#$ -o out.o
#$ -q all.q
#$ -pe openmpi 4

#switch
vasp_pot=0 #make potential
vasp_wan=1 #wannier
vasp_scf=0 #scf
vasp_bnd=0 #band

min_band=-5
max_band=2

#load modules and set environment variables
module load vasp/5.3so
export OMP_NUM_THREADS=1

#main
date
if [ $vasp_pot -eq 1 ] ; then
python vasp.py
fi
if [ $vasp_wan -eq 1 ] ;then
if [ $vasp_scf -eq 1 ] ; then
python vasp.py -scf -wannier
mpirun -np $NSLOTS vasp
fi
fi
if [ $vasp_scf -eq 1 ] ; then
python vasp.py -scf
mpirun -np $NSLOTS vasp
mv KPOINTS KPOINTS_scf
mv OUTCAR OUTCAR.scf
mv DOSCAR DOSCAR.scf
fi
if [ $vasp_bnd -eq 1 ] ; then
python vasp.py -band
mpirun -np $NSLOTS vasp
python write_band.py $min_band $max_band
mv KPOINTS KPOINTS_band
mv OUTCAR OUTCAR.band
fi
if [ $vasp_wan -eq 1 ] ; then
python vasp.py -wannier
mpirun -np $NSLOTS vasp
fi
date

#The MIT License (MIT)
#
#Copyright (c) 2016 Katsuhiro Suzuki
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
