#!/usr/bin/env python
# -*- coding:utf-8
""" This program plots band structrue and dos distribution.
 execution command is next line
 python write_band.py min_e max_e output_name
 default :: -10, 10, energy

The original code was written by Dr. Hidetomo Usui (Osaka Univ.).
Moreover the recent code cleaned and released by Katsuhiro Suzuki (Ritsumeikan Univ.).
"""

def dos_dat(ol,ef):
  f=open('POSCAR','r')
  for i in range(6):
    f.readline()
  tmp=f.readline().split()
  f.close()
  atom_num=[int(tp) for tp in tmp]
  nedos=0
  sw_pdos='F'
  for f in open('INCAR','r'):
    if f.find('NEDOS')!=-1:
      nedos=int(f.split('=')[1])
    if f.find('LORBIT')!=-1:
      sw_pdos='T'
  tmp=[f.split() for f in open('DOSCAR.scf','r')]
  tpe=[0.0 for i in range(nedos)]
  f=open('dos.dat','w')
  for i in range(nedos):
    tpe[i]=float(tmp[6+i][0])-ef
    f.write('%7.3f %11s %11s\n'%(tpe[i],tmp[6+i][1],tmp[6+i][2]))
  f.close()
  if sw_pdos=='T':
    tp=[[0.0,0.0,0.0,0.0] for i in range(nedos)]
    tp1=[[0.0,0.0,0.0,0.0] for i in range(nedos)]
    for j in range(atom_num[0]):
      for i in range(nedos):
        for k in range(ol):
          tp[i][k]=tp[i][k]+float(tmp[i+(nedos+1)*j+nedos+7][k+1])
    for j in range(atom_num[1]-1):
      for i in range(nedos):
        for k in range(ol):
          tp1[i][k]=tp1[i][k]+float(tmp[i+(nedos+1)*j
                                        +(nedos+1)*(atom_num[0]+1)+6][k+1])
    f=open('pdos.dat','w')
    for i in range(nedos):
      f.write('%7.3f %11.4e %11.4e %11.4e %11.4e\n'%(
          tpe[i],tp[i][0],tp[i][1],tp[i][2],tp[i][3]))
    f.close()
    f=open('pdos1.dat','w')
    for i in range(nedos):
      f.write('%7.3f %11.4e %11.4e %11.4e %11.4e\n'%(
          tpe[i],tp1[i][0],tp1[i][1],tp1[i][2],tp1[i][3]))
    f.close()

def str_to_float(str):
  buf=str.strip().split()
  ret = [float(bf) for bf in buf]
  return ret

def str_to_int(str):
  buf=str.strip().split()
  ret = [int(bf) for bf in buf]
  return ret

def naiseki(a1,a2):
  return sum(b1*b2 for b1,b2 in zip(a1,a2))

def gaiseki(a1,a2):
  return [a1[1]*a2[2]-a1[2]*a2[1], a1[2]*a2[0]-a1[0]*a2[2], a1[0]*a2[1]-a1[1]*a2[0]]

def div_list(a,b):
  return [aa/b for aa in a]

def multi_list(a,b):
  return [aa*b for aa in a]

def sum_list(a,b):
  return [aa+bb for aa,bb in zip(a,b)]

def diff_list(a,b):
  return [aa-bb for aa,bb in zip(a,b)]

def get_Fermi_Energy():
  ierr=os.system('grep "E-fermi" OUTCAR.scf > EF.txt')
  if ierr != 0:
    print ierr
    print '!!! grep error !!! fermi_energy is set 0'
    print 'ef'
    ef=0.0
  else:
    grep=open('EF.txt')
    ef=float(((grep.readline()).strip()).split()[2])
    print 'ef'
    print ef
  return(ef)

import os, math, sys, time
f1=open('EIGENVAL')
f2=open('CHGCAR')

#read lattice constants from CHGCAR
system_name=f2.readline().strip()
unit_length=float(f2.readline().strip())

print system_name
print unit_length

a1=multi_list(str_to_float(f2.readline()),unit_length)
a2=multi_list(str_to_float(f2.readline()),unit_length)
a3=multi_list(str_to_float(f2.readline()),unit_length)

print 'lattice structure from CHGCAR'
print a1
print a2
print a3

volume=naiseki(a1,gaiseki(a2,a3))

print 'volume'
print volume

b1=div_list(gaiseki(a2,a3),volume)
b2=div_list(gaiseki(a3,a1),volume)
b3=div_list(gaiseki(a1,a2),volume)

print ('reciprocal lattice vector')
print b1
print b2
print b3

#get fermi energy
ef=get_Fermi_Energy()

#read EIGENVAL
for i in range(0, 5):
  line=f1.readline() # kara yomi

buf=str_to_int(f1.readline())
k_point_num=buf[1]
band_num=buf[2]
print 'k_point_num, band_num'
print k_point_num, band_num


ene=open('energy.dat','w')

kp=open('KPOINTS')
line=kp.readline()

k_point_line_num=float(kp.readline().strip())

line=kp.readline()
line=kp.readline()
k_position_name=[kp.readline().strip().split()[4]]

for i in range(0, int(float(k_point_num)/float(k_point_line_num))):
  k_position_name.append(kp.readline().strip().split()[4])
  line=kp.readline()
  line=kp.readline()

k_length=0.0
k_length_point=[0.0]
for i in range(0, k_point_num):
  if i % k_point_line_num == 0:
    k_length_point.append(k_length)

#  ene.writelines('\n')
  line=f1.readline() #kara yomi

  k_point_now_rec=str_to_float(f1.readline())

  k_point_now=sum_list(multi_list(b1,k_point_now_rec[0]),multi_list(b2,k_point_now_rec[1]))
  k_point_now=sum_list(k_point_now,multi_list(b3,k_point_now_rec[2]))

  if i == 0:
    k_point_before=k_point_now

  k_point_diff=diff_list(k_point_now,k_point_before)
  k_length_diff=math.sqrt(naiseki(k_point_diff,k_point_diff))
  k_length = k_length + k_length_diff

  k_point_before=k_point_now

  ene.writelines(str(k_length))
  ene.writelines(' ')


  for j in range(0, band_num):
    energy=str_to_float(f1.readline())
    energy[1]=energy[1]-ef

    ene.writelines(str(energy[1]))
    ene.writelines(' ')

  ene.writelines('\n')

ene.close()

k_length_point.append(k_length)

if len(sys.argv) >= 3 :
  max_e=float(sys.argv[2])
  min_e=float(sys.argv[1])
else:
  max_e=10
  min_e=-10

if len(sys.argv) == 4:
  output_name = sys.argv[3]
else:
  output_name = "energy"

write_file_name = "energy.dat"

gnu= 'unset key\n'

for i in k_length_point:
  gnu = gnu + 'set arrow from ' + str(i) + ', ' + str(min_e) + ' to ' + str(i) + ', ' + str(max_e) + ' nohead\n'

gnu = gnu + 'set xtics ('
for i in range(0,len(k_position_name)-1):
  gnu = gnu + '" ' + k_position_name[i] + '" ' + str(k_length_point[i+1]) + ',' 

gnu = gnu + '" ' + k_position_name[len(k_position_name)-1] + '" ' + str(k_length_point[len(k_position_name)]) + ')\n'

gnu = gnu + 'set ylabel "Energy [eV]"\n'

gnu = gnu + 'set term postscript color\n'
gnu = gnu + 'set output "' + output_name + '.eps"\n'
gnu = gnu + 'set xrange[0:' + str(k_length) + ']\n'
gnu = gnu + 'set yrange[' + str(min_e) + ':' + str(max_e) + ']\n' 
gnu = gnu + 'plot "' + write_file_name + '" u 1:2 w lp pt 7 ps 0.5 lt 1'

if band_num != 1:
 gnu = gnu + ','

for i in range(3, band_num+2):
  if i == band_num + 1: 
    gnu = gnu + '"' + write_file_name + '" u 1:' + str(band_num+1) + ' w lp pt 7 ps 0.5 lt 1\n'
  else:
    gnu = gnu + '"' + write_file_name + '" u 1:' + str(i) + ' w lp pt 7 ps 0.5 lt 1,'

#print(gnu)

gp = os.popen('gnuplot', 'w')
print >> gp, gnu
print >> gp, 'quit'
gp.flush()
gp.close()

f=open('band.dat','w')
for i in open('energy.dat','r'):
  tmp=i.split()
  for j in range(1,len(tmp)):
    f.write('%16.14f %11.6f\n'%(float(tmp[0]),float(tmp[j])))
f.close()

dos_dat(3,ef)

__version__='1.1'
__license__="""Copyright (c) 2013 Hidetomo Usui, 2016 Katsuhiro Suzuki
Released under the MIT license
http://opensource.org/licenses/mit-license.php
"""
