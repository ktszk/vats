#!/usr/bin/env python
# -*- coding:utf-8
#This program is INCAR, POTCAR, KPOINTS generator from POSCAR file for VASP calculation.
#The original code was written by Dr. Katsuhiro Suzuki (Ritsumeikan Univ.)
from __future__ import division,print_function,unicode_literals
if __name__=="__main__":
    (T,F)=(True, False)
    #the Number of mpi threads, but unfortunately this variable doesn't work.
    num_mpi=4

    #the name of this calculation
    name='FeSe'
    #the list of VCA weight
    vca=[]

    #if you want to consider f orbitals core_in_f=0
    core_in_f=1
    #the name of hydrogen pseudo potential whtat you use this calculation
    hcha='1.75'
    #pseudo potential directory name
    exc='PBE_54'
    #pseudo potential directory path
    potpath='/home/Apps/VASP/%s/'
    #the name of exchange correspond functional for GGA tag
    ggatag='PBEsol'
    #setting k-mesh for scf calc., band clac. and wannierization
    knum_scf=10
    knum_band=20
    knum_wan=8
    #cut off energy 
    encut=550 #eV
    #the band number
    nbands=100

    #maximum iteration
    nelm=40
    #minimum iteration
    nelmin=2
    #switch of LDA+U
    ldau=F
    #setting type of LDA+U calculation
    ldaU_type=1
    #select atom which was considered U
    #set elements of ldauL as -1: no consideration U 1:p, 2:d, 3:f for each atoms
    ldauL=[3,-1,-1,-1,-1]
    #the list of U
    ldauU=[6.5,0.0,0.0,0.0,0.0]
    #the list of J
    ldauJ=[0.0,0.0,0.0,0.0,0.0]
    #switch of output LDA+U
    ldau_print=1

    #the switch to using stress tensor
    sw_press=F
    #value of Pressure. the unit is kbar (0.1GPa)
    Pstress=00
    #Optimization option see vasp manual 6.24 ISIF
    isif=3
    #maximum iteration of the optimization
    nsw=200
    #threshold of difference of energy
    EDIFFG=-7

    #switch spin orbital coupling calculation
    soc=F

    nedos=0
    lorbit=0

    #the number of wannier functions
    nwan=3
    #maximum/minimum range of dis window
    w_max=0.4 #6.0 #full_d
    w_min=-0.35 #-6.0 #full_d
    #maximum/minimum range of frozen window
    f_max=0.2
    f_min=-0.2  #-2.1
    #initial wannier projection
    #!!!cation!!! elements of projections list is tuple
    projections=[('f=-0.5,0.5,0.5','dxz,dyz,dxy')]

    #switch of setting maximum iteration
    witr=T
    #maximum iteration of wannierization
    iter_num=400
    #switch of considering spinors degree of freedom
    spinors=F
    #switch of output fermi surface for .bxsf
    out_FS=T
    #mesh for bxsf file
    FS_point_num=100
    #output wannier function
    out_wan=T

    #switch of output xyz files
    sw_xyz=T
    THC=T #translate home cell
    gcent=T

    #k-point list for band calculation
    point=[['GAMMA',0.,0.,0.],['L',0.,0.5,0.],['W',0.5,0.75,0.25],
           ['X',0.5,0.5,0.],['GAMMA',0.,0.,0.]]

"""
    point=[['GAMMA',0.0,0.0,0.0],['Y',0.5,-0.5,0.0],
           ['GAMMA',1.0,0.0,0.0],['Z',1.0,0.0,0.5],
           ['T',0.5,-0.5,0.5],['Z',0.0,0.0,0.5]]
    point=[['GAMMA',0.0,0.0,0.0],['X',0.5,0.0,0.0],
           ['M',0.5,0.5,0.0],['GAMMA',0.0,0.0,0.0],
           ['Z',0.0,0.0,0.5],['R',0.5,0.0,0.5],
           ['A',0.5,0.5,0.5],['Z',0.0,0.0,0.5]]
"""

import os,sys
def read_poscar(swt):
    a=[0]*3
    fposcar=open('POSCAR','r')
    fposcar.readline()
    fposcar.readline()
    for i in range(3):
        tmp=fposcar.readline().split()
        a[i]=[float(tp) for tp in tmp]
    atom=fposcar.readline().split()
    if swt!=1:
        tmp=fposcar.readline().split()
        atom_num=[int(temp) for temp in tmp]
        fposcar.readline()
        atom_list=[]
        for i,atm in enumerate(atom):
            for j in range(atom_num[i]):
                tmp=fposcar.readline().split()
                tmp=[float(tp) for tp in tmp]
                atom_list.append([atm]+tmp)
        fposcar.close()
        return(a,atom_list)
    else:
        fposcar.close()
        return(atom)
def select_gga(gga):
    if gga in ['PB','Perdew-Becke']:
        ngga='PB'
    elif gga in ['PW86','86','PW','Perdew-Wang 86']:
        ngga='PW'
    elif gga in ['PW91','91','Perdew-Wang 91']:
        ngga='91'
    elif(gga in ['rPBE','RPBE','revised PBE','Revised PBE','RP',
                 'Revised Perdew-Burke-Ernzerhof']):
        ngga='RP'
    elif(gga in ['PBEsol','PBE-Sol','PS','PBE-sol','PBESol',
                 'Perdew-Burke-Ernzerhof revised for solids']):
        ngga='PS'
    else:
        ngga='PE'
    return ngga
def make_potcar(exchange,core_in_f,hcha,potpath):
    def coref(atom):
        f3_list={'Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy',
                 'Ho','Er','Tm','Lu'}
        f2_list={'Eu','Er','Yb'}
        for i,atm in enumerate(atom):
            for FL in f3_list:
                if(atm==FL):
                    atom[i]=atm+'_3'
        return(0)
    def defsvpv(atom,pv):
        sv_list={'Ba','Ca','Cs','K','Nb','Ra','Rb','Sr','Y','Zr'}
        pv_list={'Ca','K','Nb','Rb'}
        for i,atm in enumerate(atom):
            if pv==1:
                for pvl in pv_list:
                    if atm==pvl:
                        atom[i]=atm+'_pv'
            for svl in sv_list:
                if atm==svl:
                    atom[i]=atm+'_sv'
        return(0)
    def selectH(atom,hcha):
        for i,atm in enumerate(atom):
            if atm=='H':
                atom[i]=atm+hcha
        return(0)
    stdo='begin to make POTCAR'
    atom=read_poscar(1)
    stdo=stdo+'\nthis material contains\n   '
    for atm in atom:
        stdo=stdo+atm+' '
    info=defsvpv(atom,1)
    if core_in_f==1:
        stdo=stdo+'\nf orbitals are kept frozen in core'
        info=coref(atom)
    if((hcha != 0) and ('H' in atom)):
        stdo=stdo+'\n H pot use H'+hcha
        info=selectH(atom,hcha)
    pot_link=potpath%exchange
    opt=''
    for atm in atom:
        opt=opt+'%s%s/POTCAR '%(pot_link,atm)
    opt=opt+'>POTCAR'
    os.system('cat %s'%opt)
    stdo=stdo+'\nfinish to make POTCAR'
    print(stdo)

def make_incar(sysname,istart,icharg,nelm,nelmin,encut,
               gga,nbands,vca,nedos,wan,out_wan):
    prec_flag=1
    ispin=1
    ediff=-6
    if prec_flag==1:
        prec='Accurate'
    else:
        prec='Normal'
    f=open('INCAR','w')
    f.write('''SYSTEM = %s
PREC   = %s
ENCUT  = %d
ISTART = %d
ICHARG = %d
ISPIN  = %d
NELM   = %d
NELMIN = %d
NBANDS = %d
EDIFF  = 1E%d
GGA    = %s\n'''
            %(sysname,prec,encut,istart,icharg,
              ispin,nelm,nelmin,nbands,ediff,gga))
    if soc:
        f.write('LSORBIT = .True.\n')
    if ldau:
        f.write('LASPH  = .True.\n')
        f.write('LDAU   = .True.\nLDAUTYPE = %d\n'%ldaU_type)
        if len(ldauL)!=0:
            f.write('LDAUL  = ')
            for d in ldauL:
                f.write('%d '%d)
            f.write('\nLDAUU  = ')
            for d in ldauU:
                f.write('%4.2f '%d)
            f.write('\nLDAUJ  = ')
            for d in ldauJ:
                f.write('%4.2f '%d)
            f.write('\n')
            if ldau_print!=0:
                f.write('LDAUPRINT = %d\n'%ldau_print)
    if sw_press:
        f.write('PSTRESS = %d\nIBRION = 1\nISIF = %d\n'%(Pstress,isif))
        f.write('NSW = %d\nEDIFFG= 1E%d\n'%(nsw,EDIFFG))
    if(nedos!=0):
        f.write('NEDOS  = %d\nISMEAR = -5\n'%nedos)
    if(lorbit!=0):
        f.write('LORBIT = %d\n'%lorbit)
    if(len(vca)!=0):
        f.write('VCA    =')
        for i in vca:
            f.write(' %4.2f'%i)
        f.write('\n')
    if(wan==1):
        f.write('''LWANNIER90 = .True.
LWRITE_MMN_AMN = .True.
LWANNIER90_RUN = .True.\n''')
    if out_wan:
        f.write('LWRITE_UNK = .True.\n')
    f.close()
def make_kpoints(n,specific_point,band):
    f=open('KPOINTS','w')
    f.write('KPOINTS\n')
    if band==1:
        f.write('%d\n'%n)
        f.write('Line-mode\nRec\n')
        for sp,sp2 in zip(specific_point[:len(specific_point)-1],
                          specific_point[1:]):
            f.write('  %f  %f  %f ! %s\n'%tuple(sp[1:]+sp[:1]))
            f.write('  %f  %f  %f ! %s\n'%tuple(sp2[1:]+sp2[:1]))
            if sp!=specific_point[len(specific_point)-2]:
                f.write('\n')
    else:
        f.write('0\nGamma\n')
        f.write(' %d  %d  %d\n  0   0   0\n'%(n,n,n))
    f.close()
def make_win(nbands,point,nwan,win_max,win_min,frz_max,frz_min,
             projections,knum_wan,out_FS,out_wan):
    def read_files():
        (a,atom_list)=read_poscar(0)
        tmp=0; klist=''
        for klf in open('wannier90.win','r'):
            if klf.find('kpoints')==-1:
                if tmp==1:
                    klist=klist+klf
            else:
                if klf.find('begin kpoints')!=-1:
                    tmp=1
                elif klf.find('end kpoints')!=-1:
                    tmp=0
        fef=open('EF.txt','r')
        tmp=fef.readline().split()
        ef=float(tmp[2])
        fef.close()
        return(a,atom_list,ef,klist)
    (a,atom_list,ef,klist)=read_files()
    f=open('wannier90.win','w')
    f.write('''num_bands = %5d  ! set to NBANDS by VASP
num_wann = %5d\n'''%(nbands,nwan))
    if witr:
        f.write('num_iter = %5d\n'%iter_num)
    if spinors:
        f.write('spinors = True\n')

    f.write('''\ndis_win_max  = %7.4f
dis_win_min  = %7.4f
dis_froz_max = %7.4f
dis_froz_min = %7.4f\n\n'''
            %(ef+win_max,ef+win_min,ef+frz_max,ef+frz_min))
    f.write('Begin Kpoint_Path\n')
    for pt,pt2 in zip(point[:len(point)-1],point[1:]):
        if pt=='GAMMA':
            pt[0]='G'
        if pt2[0]=='GAMMA':
            pt2[0]='G'
        f.write('%s %5.2f %5.2f %5.2f  %s %5.2f %5.2f %5.2f\n'%tuple(pt+pt2))
    f.write('End Kpoint_Path\n\n')
    f.write('use_bloch_phases = %s\n'%'False'
            +'fermi_energy =%7.4f\n'%ef
            +'bands_plot = %s\n'%'True')
    if sw_xyz:
        f.write('\nwrite_xyz = True\n')
        if THC:
            f.write('translate_home_cell = True\n')
    if out_FS:
        f.write('\nfermi_surface_plot = %s\n'%out_FS)
        f.write('fermi_surface_num_points = %d\n'%FS_point_num)
    if out_wan:
        f.write('wannier_plot = %s\n'%out_wan)
    f.write('\nbegin unit_cell_cart\n')
    for axis in a:
        f.write('%14.7f %13.7f %13.7f\n'%tuple(axis))
    f.write('end unit_cell_cart\n\n')
    f.write('begin atoms_frac\n')
    for alist in atom_list:
        f.write('%-2s %15.7f %13.7f %13.7f\n'%tuple(alist))
    f.write('end atoms_frac\n\n')
    f.write('begin projections\n')
    for prj in projections:
        f.write('%s:%s\n'%prj)
    f.write('end projections\n\n')
    if gcent:
        f.write('guiding_centres = True\n')
    f.write('mp_grid = %5d %5d %5d\n\nbegin kpoints\n'%tuple([knum_wan]*3))
    f.write(klist)
    f.write('end kpoints\n')
    f.close()
def main(nelm,nelmin,nedos,knum_scf):
    flag_scf=0; flag_dos=0; flag_band=0; flag_run=0; wan=0
    gga=select_gga(ggatag)
    if(len(sys.argv)>1):
        for sargv in sys.argv:
            if(sargv.find('-scf')!=-1):
                flag_scf=1
            elif(sargv.find('-band')!=-1):
                flag_band=1
            elif(sargv.find('-wannier')!=-1 or
                 sargv.find('-wan')!=-1):
                wan=1
            elif(sargv.find('-dos')!=-1):
                flag_dos=1
    if(flag_run==1):
        vasprun='mpirun -np %d vasp'%num_mpi
    if(flag_scf==0 and flag_dos==0 and flag_band==0 and wan==0):
        make_potcar(exc,core_in_f,hcha,potpath)
    if(flag_scf==1):
        if wan==1:
            knum_scf=knum_wan
            nelm=1
            nelmin=0
            nedos=0
        make_incar(name,0,2,nelm,nelmin,encut,gga,nbands,vca,nedos,wan,False)
        make_kpoints(knum_scf,point,0)
        if(flag_run==1):
            os.system(vasprun)
    if(flag_dos==1):
        make_incar(name,1,11,nelm,nelmin,encut,gga,nbands,vca,nedos,0,False)
        make_kpoints(knum_scf,point,0)
    if(flag_band==1):
        make_incar(name,1,11,nelm,nelmin,encut,gga,nbands,vca,0,0,False)
        make_kpoints(knum_band,point,1)
        if(flag_run==1):
            os.system(vasprun)
    if(flag_scf==0 and wan==1):
        if os.path.isfile('wannier90.win'):
            make_win(nbands,point,nwan,w_max,w_min,f_max,f_min,
                     projections,knum_wan,out_FS,out_wan)
        make_incar(name,1,11,nelm,nelmin,encut,gga,nbands,vca,0,1,True)
        make_kpoints(knum_wan,point,0)
if __name__=="__main__":
    main(nelm,nelmin,nedos,knum_scf)

__version__='1.1'
__license__="""Copyright (c) 2016 Katsuhiro Suzuki
Released under the MIT license
http://opensource.org/licenses/mit-license.php
"""
